<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\products;
use App\Category;
use App\product_image;
use App\product_tag;
use App\tags;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


class ProductController extends Controller
{
    //khai báo biến để echo các option danh mục
    private $htmloption='';

    public function Danhsach(){
        $products=products::orderBy('id','DESC')->paginate(5);
        return view('admin/Product/Product',['products'=>$products]);
    }

    public function Add(){

        $htmloption=$this->showCategories(0,'','');
    
        return view('Admin/Product/Add',['htmloption'=>$htmloption]);
    
    }
    //Đệ quy danh mục
    public function showCategories($parent_id = 0, $char = '',$father_id){
           
        $categories=Category::all();
        foreach ($categories as $key => $item)
        {
            
            // Nếu là chuyên mục con thì hiển thị
            if ($item['parent_id'] == $parent_id)
            {   
                if(!empty($father_id) && $item['id']==$father_id){
                    $danhmuc=Category::find($father_id);   
                    $this->htmloption.='<option value="'.$item['id'].'" selected>'.$char.$item['name'].'</option>';
                    }
        
                else{
                $this->htmloption.='<option value="'.$item['id'].'">'.$char .$item['name'] .'</option>';
                }
                // Xóa chuyên mục đã lặp
                
                 
                // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
                $this->showCategories($item['id'], $char.'--',$father_id);
            }
        }
        return $this->htmloption;
       
    }

    public function Postproduct(Request $request){
        $this->validate($request,[
            'Ten'=>'required|min:3|max:100',
            'tags'=>'required',
            'content'=>'required|min:3|max:1000',
            'price'=>'required',
            'Danhmuc'=>'required',
            'feature_image' => 'required|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_chitiet' => 'required',
            'image_chitiet.*' => 'mimes:jpeg,jpg,png,gif,svg|max:2048'

        
           ],
           [
             'Ten.required'=>'Bạn chưa nhập tên sản phẩm',
             'Ten.max'=>'Tên danh mục quá dài',
             'Ten.min'=>'Tên danh mục quá ngắn',
             'price.required'=>'Bạn chưa nhập giá sản phẩm',
             'Danhmuc.required'=>'Bạn chưa chọn danh mục sản phẩm',
             'content.required'=>'Bạn chưa nhập nội dung',
             'content.max'=>'Nội dung quá dài',
             'content.min'=>'Nội dung quá ngắn',
             'feature_image.required'=>'Bạn chưa chọn ảnh nổi bật',
             'feature_image.mimes'=>'Vui lòng chọn đúng định dạng ảnh nổi bật',
             'feature_image.max'=>'Kích thước ảnh quá lớn',
             'image_chitiet.*.mimes'=>'Vui lòng chọn đúng định dạnh ảnh chi tiết',
             'image_chitiet.required'=>'Hãy chọn ít nhất 1 ảnh chi tiết',
             'tags.required'=>'Bạn chưa nhập tags'
             
           ]);
            
        try{
              
                DB::beginTransaction();
                $product=new products;
                $product->name=$request->Ten;
                $product->price=$request->price;
                $product->category_id=$request->Danhmuc;
                $product->content=$request->content;
                $product->user_id=Auth::user()->id;
                
                //Image nổi bật
                
                if($request->hasFile('feature_image')){
                $feature_image_name= $request->feature_image->getClientOriginalName();
                $path = $request->file('feature_image')->storeAs('public/photos/product', $feature_image_name);
                $product->image_name=$feature_image_name;
                $product->feature_image=Storage::url($path);
                }
                $product->save();
                $product_id=$product->id;
                //Ảnh chi tiết
                
                if($request->hasFile('image_chitiet')){  
                    foreach ($request->file('image_chitiet') as $file) {
                        $product_images=new product_image;  
                        $image_chitiet_name=$file->getClientOriginalName();
                        $link_image=$file->storeAS('public/photos/product',$image_chitiet_name);
                    $product_images->image=Storage::url($link_image); 
                    $product_images->image_name=$image_chitiet_name;   
                    
                $product_images->product_id=$product_id;    
                $product_images->save();   
                    } 
                }
                //tags
                foreach($request->tags as $tags_item){
                $tags_insert=tags::firstOrCreate(['name'=>$tags_item]);
                $product_tags=new product_tag;
                $product_tags->product_id=$product_id;
                $product_tags->tag_id=$tags_insert->id;
                $product_tags->save();
                
                }
                DB::commit();
        return redirect('admin/product/danhsach')->with('thongbao','Bạn đã thêm sản phẩm thành công');
                
        }
        catch(\Exception $exception){
            DB::rollBack();
            Log::error($exception->getMessage().$exception->getLine());
        }

       
}
    public function Getsua($id){
        $product=products::find($id);
        $father_id=$product['category_id'];
        $htmloption=$this->showCategories(0,'',$father_id);
    
        return view('Admin/Product/Sua',['product'=>$product,'htmloption'=>$htmloption]);
       
    }

    public function Postsua(Request $request,$id){
        $this->validate($request,[
            'Ten'=>'required|min:3|max:100',
            'tags'=>'required',
            'content'=>'required|min:3|max:1000',
            'price'=>'required',
            'Danhmuc'=>'required',
            'feature_image' => 'mimes:jpeg,png,jpg,gif,svg|max:2048',
            'image_chitiet.*' => 'mimes:jpeg,jpg,png,gif,svg|max:2048'

        
           ],
           [
             'Ten.required'=>'Bạn chưa nhập tên sản phẩm',
             'Ten.max'=>'Tên danh mục quá dài',
             'Ten.min'=>'Tên danh mục quá ngắn',
             'price.required'=>'Bạn chưa nhập giá sản phẩm',
             'Danhmuc.required'=>'Bạn chưa chọn danh mục sản phẩm',
             'content.required'=>'Bạn chưa nhập nội dung',
             'content.max'=>'Nội dung quá dài',
             'content.min'=>'Nội dung quá ngắn',
             'feature_image.mimes'=>'Vui lòng chọn đúng định dạng ảnh nổi bật',
             'feature_image.max'=>'Kích thước ảnh quá lớn',
             'image_chitiet.*.mimes'=>'Vui lòng chọn đúng định dạnh ảnh chi tiết',
             'tags.required'=>'Bạn chưa nhập tags'
             
           ]);
            
            try{
              
                DB::beginTransaction();
                $product=products::find($id) ;
                $product->name=$request->Ten;
                $product->price=$request->price;
                $product->category_id=$request->Danhmuc;
                $product->content=$request->content;
                $product->user_id=Auth::user()->id;
                
                //Image nổi bật
                
                if($request->hasFile('feature_image')){
                $feature_image_name= $request->feature_image->getClientOriginalName();
                $path = $request->file('feature_image')->storeAs('public/photos/product', $feature_image_name);
                $product->image_name=$feature_image_name;
                $product->feature_image=Storage::url($path);
                }
                $product->save();
                $product_id=$product->id;
                //Ảnh chi tiết
                
                if($request->hasFile('image_chitiet')){ 
                    DB::table('product_images')->where('product_id',$id)->delete(); 
                    foreach ($request->file('image_chitiet') as $file) {
                        $product_images=new product_image;  
                        $image_chitiet_name=$file->getClientOriginalName();
                        $link_image=$file->storeAS('public/photos/product',$image_chitiet_name);
                    $product_images->image=Storage::url($link_image); 
                    $product_images->image_name=$image_chitiet_name;   
                    
                $product_images->product_id=$product_id;    
                $product_images->save();   
                    } 
                }
                //tags
                foreach($request->tags as $tags_item){
                $tag_id[]=tags::firstOrCreate(['name'=>$tags_item])->id;
                $product->tags()->sync($tag_id);      
                }
                DB::commit();
        return redirect('admin/product/danhsach')->with('thongbao','Bạn đã sửa sản phẩm thành công');
                
        }
            catch(\Exception $exception){
               DB::rollBack();
               Log::error($exception->getMessage().$exception->getLine());
        }

    }
   
    public function Xoa($id){
           $product=products::find($id);
           $product->delete();
        return redirect('admin/product/danhsach')->with('thongbao','Bạn đã xóa sản phẩm thành công');
        
    }

}
