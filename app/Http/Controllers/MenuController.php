<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use Illuminate\Support\Str;
class MenuController extends Controller
{
    //
    private $htmloption='';
    

    public function Danhsach(){
        $menus=Menu::orderBy('id','DESC')->paginate(5);
        return view('admin/menu/Menu',['menus'=>$menus]);
    }
    public function Add(){

        $htmloption=$this->showCategories(0,'','');
    
        return view('Admin/Menu/Add',['htmloption'=>$htmloption]);
    
    }

    public function showCategories($parent_id = 0, $char = '',$father_id){

        $menus=Menu::all();
        foreach ($menus as $key => $item)
        {
            
            // Nếu là chuyên mục con thì hiển thị
            if ($item['parent_id'] == $parent_id)
            {   
               
               if(!empty($father_id) && $item['id']==$father_id){   
                      $this->htmloption.='<option value="'.$item['id'].'" selected>'.$char.$item['name'].'</option>';
                    }
                   
               else{
                $this->htmloption.='<option value="'.$item['id'].'">'.$char .$item['name'] .'</option>';
               }
                // Xóa chuyên mục đã lặp
                
                 
                // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
                $this->showCategories($item['id'], $char.'--',$father_id);
            }
        }
        return $this->htmloption;
       
    }

    public function Postmenu(Request $request){
        $this->validate($request,[
         'Ten'=>'required|unique:categories,name|min:3|max:100',
        ],
        [
          'Ten.required'=>'Bạn chưa nhập tên menu',
          'Ten.max'=>'Tên menu quá dài',
          'Ten.min'=>'Tên menu quá ngắn',
          'Ten.unique'=>'Trùng tên menu'
        ]);
        $menus=new Menu;
        $menus->name=$request->Ten;
        $menus->slug=str::slug($request->Ten);
        if($request->Danhmuc==''){
            $menus->parent_id=0;
        }
        else{
            $menus->parent_id=$request->Danhmuc;
        }
        $menus->save();
        return redirect('admin/menu/danhsach')->with('thongbao','Bạn đã thêm menu thành công!');
 
    }

    public function Getsua($id){
        $menuname=Menu::find($id);
        $father_id=$menuname['parent_id'];
        
 
     $htmloption=$this->showCategories(0,'',$father_id);
 
        return view('Admin/Menu/Sua',['menuname'=>$menuname,'htmloption'=>$htmloption]);
     }

     public function Postsua(Request $request,$id){
        $this->validate($request,[
            'Ten'=>'required|min:3|max:100',
           ],
           [
             'Ten.required'=>'Bạn chưa nhập tên danh mục',
             'Ten.max'=>'Tên danh mục quá dài',
             'Ten.min'=>'Tên danh mục quá ngắn',
             
           ]);
           
           $menus=menu::find($id);
           if(!empty($request->Ten)){
           
           $menus->name=$request->Ten;
           }
           $menus->slug=str::slug($request->Ten);
           if($request->Danhmuc==''){
               $menus->parent_id=0;
           }
           else{
               $menus->parent_id=$request->Danhmuc;
           }
           $menus->save();
           return redirect('admin/menu/danhsach')->with('thongbao','Bạn đã sửa menu thành công!');
    }

    public function Xoa($id){
        $menu=Menu::find($id);
        $menus=Menu::all();
        foreach($menus as $key => $danhmuc){
            if($id==$danhmuc['parent_id']){
                   $danhmuc->delete();
            }
        }
        $menu->delete();
        return redirect('admin/menu/danhsach')->with('thongbao','Đã xóa thành công menu');
     }

}
