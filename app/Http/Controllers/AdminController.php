<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Admin;
class AdminController extends Controller
{
    //
    public function Getlogin(){
        return view('Admin/Admin/Login');
    }

    public function Postlogin(Request $request){
       $this->validate($request,
       [
          'email'=>'required|min:3|max:100',
          'password'=>'required'

       ],
       [
          'email.required'=>'Bạn chưa nhập email',
          'email.min'=>'Địa chỉ Email quá ngắn',
          'email.max'=>'Địa chỉ Email quá dài',
          'password.required'=>'Bạn chưa nhập mật khẩu'
       ]);
       
       if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){

        return redirect('admin/categories/danhsach')->with('thongbao','Đăng nhập thành công');
       }
       else{
       return redirect('admin/login')->with('thongbao','Bạn đã nhập sai email hoặc mật khẩu');
       }
    }

    public function Getlogout(){
        Auth::logout();
        return redirect('/login')->with('thongbao','Bạn đã đăng xuất');
    }
}
