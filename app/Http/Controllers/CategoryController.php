<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Str;
class CategoryController extends Controller
{   
    private $htmloption;
    public function __construct(){
           $this->htmloption='';
        
    }
    //
    public function Danhsach(){
        $categories=Category::orderBy('id','DESC')->paginate(5);
        return view('admin/category/Category',['categories'=>$categories]);
    }
    
    public function Add(){

    $htmloption=$this->showCategories(0,'','');

    return view('Admin/Category/Add',['htmloption'=>$htmloption]);

}

    public function showCategories($parent_id = 0, $char = '',$father_id){

    $categories=Category::all();
    foreach ($categories as $key => $item)
    {
        
        // Nếu là chuyên mục con thì hiển thị
        if ($item['parent_id'] == $parent_id)
        {   
           
           if(!empty($father_id) && $item['id']==$father_id){   
                  $this->htmloption.='<option value="'.$item['id'].'" selected>'.$char.$item['name'].'</option>';
                }
               
           else{
            $this->htmloption.='<option value="'.$item['id'].'">'.$char .$item['name'] .'</option>';
           }
            // Xóa chuyên mục đã lặp
            
             
            // Tiếp tục đệ quy để tìm chuyên mục con của chuyên mục đang lặp
            $this->showCategories($item['id'], $char.'--',$father_id);
        }
    }
    return $this->htmloption;
   
}
   
    public function Postdanhmuc(Request $request){
       $this->validate($request,[
        'Ten'=>'required|unique:categories,name|min:3|max:100',
       ],
       [
         'Ten.required'=>'Bạn chưa nhập tên danh mục',
         'Ten.max'=>'Tên danh mục quá dài',
         'Ten.min'=>'Tên danh mục quá ngắn',
         'Ten.unique'=>'Trùng trên danh mục'
       ]);
       $categories=new Category;
       $categories->name=$request->Ten;
       $categories->slug=str::slug($request->Ten);
       if($request->Danhmuc==''){
           $categories->parent_id=0;
       }
       else{
           $categories->parent_id=$request->Danhmuc;
       }
       $categories->save();
       return redirect('admin/categories/danhsach')->with('thongbao','Bạn đã thêm danh mục thành công!');

   }

    
    public function Getsua($id){
       $categoryname=Category::find($id);
       $father_id=$categoryname['parent_id'];
       

    $htmloption=$this->showCategories(0,'',$father_id);

       return view('Admin/Category/Sua',['categoryname'=>$categoryname,'htmloption'=>$htmloption]);
    }

    public function Postsua(Request $request,$id){
        $this->validate($request,[
            'Ten'=>'required|min:3|max:100',
           ],
           [
             'Ten.required'=>'Bạn chưa nhập tên danh mục',
             'Ten.max'=>'Tên danh mục quá dài',
             'Ten.min'=>'Tên danh mục quá ngắn',
             
           ]);
           
           $categories=Category::find($id);
           if(!empty($request->Ten)){
           
           $categories->name=$request->Ten;
           }
           $categories->slug=str::slug($request->Ten);
           if($request->Danhmuc==''){
               $categories->parent_id=0;
           }
           else{
               $categories->parent_id=$request->Danhmuc;
           }
           $categories->save();
           return redirect('admin/categories/danhsach')->with('thongbao','Bạn đã sửa danh mục thành công!');
    }

    public function Xoa($id){
        $category=Category::find($id);
        $categories=Category::all();
        foreach($categories as $key => $danhmuc){
            if($id==$danhmuc['parent_id']){
                   $danhmuc->delete();
            }
        }
        $category->delete();
        return redirect('admin/categories/danhsach')->with('thongbao','Đã xóa thành công danh mục');
     }
 

}
