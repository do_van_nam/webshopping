<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 
class products extends Model
{
    //
    use SoftDeletes;
    protected $table='products';
    public function category(){
        return $this->belongsTo('App\Category', 'category_id');
    }
    public function product_image(){
        return $this->hasMany('App\product_image','product_id');
    }
    public function tags(){
        return $this->belongsToMany('App\tags', 'product_tags', 'product_id', 'tag_id');
    }

    
}
