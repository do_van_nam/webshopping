<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('home');
});
//Admin
Route::get('/login',[
    'as' => 'login',
    'uses' => 'AdminController@Getlogin' 
] ); 

Route::post('/login',[
    'as' => 'postlogin',
    'uses' => 'AdminController@Postlogin' 
] );  

Route::get('/logout',[
    'as' => 'logout',
    'uses' => 'AdminController@Getlogout' 
] ); 

Route::group(['prefix' => 'laravel-filemanager'], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
  });
 


Route::group(['prefix' => 'admin','middleware'=>'adminlogin'], function () {
    
Route::prefix('categories')->group(function () {
    Route::get('/danhsach',[
        'as' => 'categories.danhsach',
        'uses' => 'CategoryController@Danhsach' 
    ] );
    
    Route::get('/add',[
        'as' => 'categories.add',
        'uses' => 'CategoryController@Add' 
    ] );

    Route::post('/postdanhmuc',[
        'as' => 'categories.postdanhmuc',
        'uses' => 'CategoryController@Postdanhmuc' 
    ] );
    
    Route::get('/xoa/{id}',[
        'as' => 'categories.xoa',
        'uses' => 'CategoryController@Xoa' 
    ] );

    Route::get('/sua/{id}',[
        'as' => 'categories.getsua',
        'uses' => 'CategoryController@Getsua' 
    ] );

    Route::post('/sua/{id}',[
        'as' => 'categories.postsua',
        'uses' => 'CategoryController@Postsua' 
    ] ); 

});

//menu

    Route::prefix('menu')->group(function () {
    Route::get('/danhsach',[
        'as' => 'menu.danhsach',
        'uses' => 'MenuController@Danhsach' 
    ] );

    Route::get('/add',[
        'as' => 'menu.add',
        'uses' => 'MenuController@Add' 
    ] );

    Route::post('/postmenu',[
        'as' => 'menu.postmenu',
        'uses' => 'MenuController@Postmenu' 
    ] );

    Route::get('/sua/{id}',[
        'as' => 'menu.getsua',
        'uses' => 'MenuController@Getsua' 
    ] );

    Route::post('/sua/{id}',[
        'as' => 'menu.postsua',
        'uses' => 'MenuController@Postsua' 
    ] ); 

    Route::get('/xoa/{id}',[
        'as' => 'menu.xoa',
        'uses' => 'MenuController@Xoa' 
    ] );

});

//sản phẩm

    Route::prefix('product')->group(function () {
    Route::get('/danhsach',[
        'as' => 'product.danhsach',
        'uses' => 'ProductController@Danhsach' 
    ] );

    Route::get('/add',[
        'as' => 'product.add',
        'uses' => 'ProductController@Add' 
    ] );

    Route::post('/postproduct',[
        'as' => 'menu.postproduct',
        'uses' => 'ProductController@Postproduct' 
    ] );
    
    Route::get('/xoa/{id}',[
        'as' => 'product.xoa',
        'uses' => 'ProductController@Xoa' 
    ] );

    Route::get('/sua/{id}',[
        'as' => 'product.getsua',
        'uses' => 'ProductController@Getsua' 
    ] );

    Route::post('/sua/{id}',[
        'as' => 'product.postsua',
        'uses' => 'ProductController@Postsua' 
    ] );

});

   

});