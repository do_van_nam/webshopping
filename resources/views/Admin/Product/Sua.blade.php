@extends('Admin/layout/index')
@section('css')
  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Sửa Sản Phẩm</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Sửa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
            
            <div class="col-md-12">
            @if(count($errors)>0)
                @foreach($errors->all() as $err)
            <div class="alert alert-danger">
               
                       {{$err}}
            </div>   
                @endforeach
            
            @endif
            <form action="admin/product/sua/{{$product->id}}" method="post" enctype="multipart/form-data">
            @csrf
  <div class="form-group">
    <label for="email">Tên</label>
    <input type="text" class="form-control col-md-6" placeholder="Nhập tên sản phẩm"  name="Ten" value="{{$product->name}}">
  </div>
  <div class="form-group">
    <label for="email">Giá</label>
    <input type="text" class="form-control col-md-6" placeholder="Nhập giá sản phẩm"  name="price" value="{{$product->price}}">
  </div>
  <div class="form-group">
    <label for="email">Ảnh nổi bật</label>
    <input type="file" id="upload_file" class="form-control-file" name="feature_image" onchange="preview_image();" />
      <span id="image_preview"  >
        <img id="feature_image" width="200px" height="300px"  src="{{$product->feature_image}}"    />
      </span>
  </div>
  <div class="form-group">
    <label for="email">Ảnh chi tiết</label>
    <input type="file" id="upload_muilti_file" class="form-control-file"  onchange="preview_muilti_image();" placeholder="Chọn các ảnh chi tiết" name="image_chitiet[]" multiple>
      <span id="muilti_image_preview"  >
         <span id="muilti_image"  >
         @foreach($product->product_image as $image_chitet)
         <img  width="200px" height="300px" src="{{$image_chitet->image}}"  />
         @endforeach
      </span>
      </span>
  </div>
  <div class="form-group">
    <div><label for="pwd">Chọn Danh Mục</label></div>
    <select class="form-control col-md-6 danhmuc_select2"  id="pwd" name="Danhmuc">
    <option value="">Chọn Danh Mục</option>
      
               {{!!$htmloption!!}}
      
    </select>
  </div>
  <div class="form-group">
    <div><label for="pwd">Chọn Tag Sản Phẩm</label></div>
    <select class="form-control col-md-6 tags_select2"  multiple name="tags[]">
       @foreach($product->tags as $the_tag)
          <option value="{{$the_tag->name}}" selected>{{$the_tag->name}}</option>
       @endforeach
    </select>
  </div>
  <div class="form-group">
    <label for="comment">Nội Dung:</label>
    <textarea class="form-control col-md-6" rows="3" id="my-editor"  name="content" >{{$product->content}}</textarea>
  </div>
  <div class="form-group form-check">
    <label class="form-check-label">
      <input class="form-check-input" type="checkbox"> Remember me
    </label>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

            </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>

  @section('js')
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
  

  <script>
     $(function(){
         $(".tags_select2").select2({
    tags: true,
    tokenSeparators: [',']
  })

     $(".danhmuc_select2").select2({
    placeholder: "Chọn danh mục",
    allowClear: true
  })
     }

);
var options = {
  filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token=',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token='
    
  };

  CKEDITOR.replace('my-editor', options);
  

  // Sử dụng FileReader để đọc dữ liệu tạm trước khi upload lên Server
  
  function preview_image() 
{
 document.getElementById("feature_image").style.display = 'none';
 var total_file=document.getElementById("upload_file").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#image_preview').append("<img width='200px' height='300px' src='"+URL.createObjectURL(event.target.files[i])+"'>" );
 }
}

function preview_muilti_image() 
{
 document.getElementById("muilti_image").style.display = 'none';
 var total_file=document.getElementById("upload_muilti_file").files.length;
 for(var i=0;i<total_file;i++)
 {
  $('#muilti_image_preview').append("<img width='200px' height='300px' src='"+URL.createObjectURL(event.target.files[i])+"'>" );
 }
}



  </script>
  @endsection

  @endsection