@extends('Admin/layout/index')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Danh Mục</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Danh Mục</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="admin/product/add" class="btn btn-success float-right m-2" >Add</a>
            </div>
            <div class="col-md-12">
            @if(session('thongbao'))
            <div class="alert alert-success">
                  {{session('thongbao')}}
            </div>
            @endif
        <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Tên Sản Phẩm</th>
      <th scope="col">Giá</th>
      <th scope="col">Hình Ảnh</th>
      <th scope="col">Danh Mục</th>
      <th scope="col">Action</th>
      
    </tr>
  </thead>
  <tbody>
  @foreach($products as $sanpham)
    <tr>
      <th scope="row">{{$sanpham->id}}</th>
      <td>{{$sanpham->name}}</td>
      <td>{{number_format($sanpham->price)}}VNĐ</td>
      <td><img src="{{$sanpham->feature_image}}" width="120px" height="160px" /></td>
      <td> {{optional($sanpham->category)->name}}  </td>
      <td>
             <a href="admin/product/sua/{{$sanpham->id}}" class="btn btn-default">Sửa</a>
             <a href="{{route('product.xoa',['id'=>$sanpham->id])}}" class="btn btn-danger delete_product"  >Xóa</a>      
      </td>
    </tr>
  @endforeach  
  </tbody>
</table>
        <div class="col-md-6">{{$products->links()}}</div>
            </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  @section('sweetalert2')
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script>
      $('a.delete_product').click(function(e){
        e.preventDefault();
        var href=$(this).attr('href');
        Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.isConfirmed) {
     
    $.ajax({
                    url: href,
                    type: 'GET',
                    success: function (data) {
                        window.location.reload();
                        
                    },
                    error: function (e) {
                        console.log(e.message);
                    }
            });
    Swal.fire(
      'Deleted!',
      'Your file has been deleted.',
      'success'
    )                  

   
  }

      })
     


    })

      

  </script>
  @endsection
  @endsection