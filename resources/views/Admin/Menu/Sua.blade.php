@extends('Admin/layout/index')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Sửa Menu</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Sửa</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
            
            <div class="col-md-12">
            @if(count($errors)>0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $err)
                       {{$err}}
                @endforeach
            </div>    
            @endif
            <form action="admin/menu/sua/{{$menuname->id}}" method="post">
            @csrf
  <div class="form-group">
    <label for="email">Tên</label>
    <input type="text" class="form-control col-md-6" placeholder="Nhập tên danh mục" id="name" name="Ten" value="{{$menuname->name}}">
  </div>
  <div class="form-group">
    <label for="pwd">Chọn Menu Cha</label>
    <select class="form-control col-md-6"  id="pwd" name="Danhmuc">
    <option value="">Chọn Menu Cha</option>
      
            {{!!$htmloption!!}}        
      
    </select>
  </div>
  <div class="form-group form-check">
    <label class="form-check-label">
      <input class="form-check-input" type="checkbox"> Remember me
    </label>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

            </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>

  @endsection