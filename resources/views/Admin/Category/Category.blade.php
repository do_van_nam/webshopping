@extends('Admin/layout/index')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Danh Mục</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Danh Mục</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <a href="admin/categories/add" class="btn btn-success float-right m-2" >Add</a>
            </div>
            <div class="col-md-12">
            @if(session('thongbao'))
            <div class="alert alert-success">
                  {{session('thongbao')}}
            </div>
            @endif
        <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Tên Danh Mục</th>
      <th scope="col">Action</th>
      
    </tr>
  </thead>
  <tbody>
  @foreach($categories as $danhmuc)
    <tr>
      <th scope="row">{{$danhmuc->id}}</th>
      <td>{{$danhmuc->name}}</td>
      <td>
             <a href="admin/categories/sua/{{$danhmuc->id}}" class="btn btn-default">Sửa</a>
             <a href="admin/categories/xoa/{{$danhmuc->id}}" class="btn btn-danger">Xóa</a>      
      </td>
    </tr>
  @endforeach  
  </tbody>
</table>
        <div class="col-md-6">{{$categories->links()}}</div>
            </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>

  @endsection